package br.com.pupposoft.course.alura.domain.orcamento;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Item implements Orcavel {
	private BigDecimal valor;

}
