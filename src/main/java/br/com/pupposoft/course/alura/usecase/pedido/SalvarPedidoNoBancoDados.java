package br.com.pupposoft.course.alura.usecase.pedido;

import br.com.pupposoft.course.alura.domain.pedido.Pedido;

public class SalvarPedidoNoBancoDados implements PedidoUseCase {

	public void executar(Pedido pedido) {
		System.out.println("Salvo no BD!");
	}
	
}
