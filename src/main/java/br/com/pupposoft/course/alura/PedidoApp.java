package br.com.pupposoft.course.alura;

import java.util.Arrays;

import br.com.pupposoft.course.alura.domain.pedido.GeraPedido;
import br.com.pupposoft.course.alura.usecase.pedido.EnviarEmailPedido;
import br.com.pupposoft.course.alura.usecase.pedido.SalvarPedidoNoBancoDados;

public class PedidoApp {

	public static void main(String[] args) {
		
		/*
		 * A regra de gerar pedido não fica no programa (main). E sim no "GeraPedido"
		 * Assim, se mudar o programa de 'main' para 'web' basta o novo chamar o "GeraPedido" 
		 * 
		 * 
		 */
		String cliente = args[0];
		
		GeraPedido gerador = new GeraPedido(cliente, Arrays.asList(new SalvarPedidoNoBancoDados(), new EnviarEmailPedido()));
		
		gerador.executa();
	}

}
