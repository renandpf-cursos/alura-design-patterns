package br.com.pupposoft.course.alura.usecase.pedido;

import br.com.pupposoft.course.alura.domain.pedido.Pedido;

public interface PedidoUseCase {

	void executar(Pedido pedido);
	
}
