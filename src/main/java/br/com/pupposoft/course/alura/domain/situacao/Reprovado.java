package br.com.pupposoft.course.alura.domain.situacao;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;

public class Reprovado extends Situacao {

	@Override
	public void finalizar(Orcamento orcamento) {
		orcamento.setSituacao(new Finalizado());
	}
}
