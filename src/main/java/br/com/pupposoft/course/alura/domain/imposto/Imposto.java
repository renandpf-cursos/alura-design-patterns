package br.com.pupposoft.course.alura.domain.imposto;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Imposto {

	/*
	 * Usando o padrão DECORATOR, um imposto passa a ter comportamento diferente dependendo da composição
	 */
	
	private Imposto outroImposto;
	
	protected abstract BigDecimal realizarCalculo(Orcamento orcamento);
	
	public BigDecimal calcular(Orcamento orcamento) {
		BigDecimal valorImposto = realizarCalculo(orcamento);
		
		BigDecimal valorOutroImposto = BigDecimal.ZERO; 
		if(outroImposto != null) {
			valorOutroImposto = outroImposto.calcular(orcamento);
		}
		
		return valorImposto.add(valorOutroImposto);
	}
	
}
