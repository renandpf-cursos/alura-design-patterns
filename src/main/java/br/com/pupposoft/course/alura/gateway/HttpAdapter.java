package br.com.pupposoft.course.alura.gateway;

import java.util.Map;

public interface HttpAdapter {

	void post(String url, Map<String, Object> dados);
	
}
