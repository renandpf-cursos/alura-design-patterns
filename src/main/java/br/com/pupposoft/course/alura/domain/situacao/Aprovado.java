package br.com.pupposoft.course.alura.domain.situacao;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;

public class Aprovado extends Situacao {

	@Override
	public BigDecimal calcularValorDescontoExtra(Orcamento orcamento) {
		return orcamento.getValor().multiply(new BigDecimal("0.02"));
	}

	@Override
	public void finalizar(Orcamento orcamento) {
		orcamento.setSituacao(new Finalizado());
	}
}
