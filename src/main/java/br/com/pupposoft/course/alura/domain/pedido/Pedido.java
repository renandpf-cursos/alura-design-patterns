package br.com.pupposoft.course.alura.domain.pedido;

import java.time.LocalDateTime;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Pedido {
	private String cliente;
	private LocalDateTime data;
	private Orcamento orcamento;

}
