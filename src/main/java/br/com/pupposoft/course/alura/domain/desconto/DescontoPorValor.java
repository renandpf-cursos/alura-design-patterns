package br.com.pupposoft.course.alura.domain.desconto;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;


public class DescontoPorValor extends Desconto {

	public DescontoPorValor(Desconto proximo) {
		super(proximo);
	}

	@Override
	protected boolean deveAplicar(Orcamento orcamento) {
		return orcamento.getValor().compareTo(new BigDecimal("500")) > 0;
	}

	@Override
	protected BigDecimal calcular(Orcamento orcamento) {
		return orcamento.getValor().multiply(new BigDecimal("0.10")).setScale(2, RoundingMode.UP);
	}

}
