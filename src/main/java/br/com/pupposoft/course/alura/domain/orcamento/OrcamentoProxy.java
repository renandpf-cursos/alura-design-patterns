package br.com.pupposoft.course.alura.domain.orcamento;

import java.math.BigDecimal;

/*
 * Neste caso, podemos fazer de 2 maneiras:
 * 1. "extends Orcamento", e sobrescrever todos os métodos de Orcamento, mas chamando os métodos de "orcamento".
 * 2. "implements Orcavel" assim, somente o metodo a ser cacheado será implementado
 * 
 * O Proxy é um padrão de projeto estrutural que permite que você forneça um substituto ou um espaço reservado para outro objeto. 
 * Um proxy controla o acesso ao objeto original, permitindo que você faça algo ou antes ou depois do pedido chegar ao objeto original.
 * 
 */
public class OrcamentoProxy implements Orcavel {

	private Orcamento orcamento;
	private BigDecimal valor;
	
	public OrcamentoProxy(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	@Override
	public BigDecimal getValor() {
		if(valor == null) {
			valor = orcamento.getValor();
		}

		return valor;
	}
	
}
