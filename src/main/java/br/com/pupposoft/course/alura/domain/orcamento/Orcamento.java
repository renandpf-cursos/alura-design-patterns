package br.com.pupposoft.course.alura.domain.orcamento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.pupposoft.course.alura.domain.situacao.EmAnalise;
import br.com.pupposoft.course.alura.domain.situacao.Finalizado;
import br.com.pupposoft.course.alura.domain.situacao.Situacao;
import lombok.Getter;
import lombok.Setter;

@Getter
public class Orcamento implements Orcavel {

	private BigDecimal valor;
	private List<Orcavel> itens;
	
	@Setter
	private Situacao situacao;

	public Orcamento() {
		this.valor = BigDecimal.ZERO;
		itens = new ArrayList<>();
		situacao = new EmAnalise();
	}
	
	public void aplicarDesconto() {
		BigDecimal descontoExtra = situacao.calcularValorDescontoExtra(this);
		valor = valor.subtract(descontoExtra);
	}
	
	public void aprovar() {
		situacao.aprovar(this);
	}

	public void reprovar() {
		situacao.reprovar(this);
	}
	public void finalizar() {
		situacao.finalizar(this);
	}

	public boolean isFinalizado() {
		return situacao instanceof Finalizado;
	}
	
	public int getQuantidadeItens() {
		return itens.size();
	}
	
	public void adicionarItem(Orcavel item) {
		valor = valor.add(item.getValor());
		itens.add(item);
	}
	
	/**
	 * Para efeito de simulação, vms imaginar que esse valor vem de uma api externa que demora alguns segundos para retornar
	 */
	public BigDecimal getValor() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		return valor;
	}

}
