package br.com.pupposoft.course.alura.domain.desconto;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;


public class DescontoPorQuantidade extends Desconto {

	public DescontoPorQuantidade(Desconto proximo) {
		super(proximo);
	}

	@Override
	protected boolean deveAplicar(Orcamento orcamento) {
		return orcamento.getQuantidadeItens() > 5;
	}

	@Override
	protected BigDecimal calcular(Orcamento orcamento) {
		return orcamento.getValor().multiply(new BigDecimal("0.10"));
	}

}
