package br.com.pupposoft.course.alura.domain.orcamento;

import java.math.BigDecimal;

public interface Orcavel {

	BigDecimal getValor();
	
}
