package br.com.pupposoft.course.alura.domain.imposto;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;

public class CalculadoraImposto {

	/*
	 * Agora com a aplicação do padrão Strategy, essa classe não cresce mais, pois ela não conhece as regras 
	 * de cada imposto.
	 * 
	 * Sendo que se surgir novos impostos, não será necessário refatorar essa classe
	 * 
	 * E cada classe (implementação) de imposto possui uma unica responsabilidade e ficou com código bem menor
	 * 
	 */
	public BigDecimal calcular(Orcamento orcamento, Imposto imposto) {
		return imposto.calcular(orcamento);
	}
}
