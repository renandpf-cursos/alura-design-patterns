package br.com.pupposoft.course.alura.domain.pedido;

import java.time.LocalDateTime;
import java.util.List;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;
import br.com.pupposoft.course.alura.usecase.pedido.PedidoUseCase;
import lombok.AllArgsConstructor;

@AllArgsConstructor()
public class GeraPedido {
	private String cliente;
	
	private List<PedidoUseCase> pedidosUseCase;
	
	public void executa() {

		//Agora com o padrão Observer todos as ações serão executadas.
		//E se uma nova surgir (como gerar nota fiscal) essa classe não precisa ser modificada
		//No caso cada "PedidoUseCase" é um observador (listener) e serão notificados dado um evento
		//Isso tb é um exemplo de inversão de controle, pois essa classe não precisa se preocupar em saber quem ela vai chamar (ela só chama a lista)
		
		/*
		 * O Observer é um padrão de projeto comportamental que permite que você defina um mecanismo de assinatura para 
		 * notificar múltiplos objetos sobre quaisquer eventos que aconteçam com o objeto que eles estão observando.
		 * 
		 * Esta classe aqui por exemplo seria o 'publish'
		 * 
		 */

		/*
		 * [FACADE] - Esta classe acabou que também implementa o padrão Facade.
		 * Este padrão, em resumo, encapsula detalhes de várias passos a serem executados
		 * da classe/método 'cliente' (ou chamadora).
		 * 
		 * Em suma, o FACADE acaba sendo um oquestrador
		 * 
		 */
		
		Orcamento orcamento = new Orcamento();
		Pedido pedido = new Pedido(cliente, LocalDateTime.now(), orcamento);
		pedidosUseCase.forEach(useCase -> useCase.executar(pedido));
	}
}
