package br.com.pupposoft.course.alura.domain.desconto;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;

public class CalculadoraDesconto {
	
	
	/*
	 *	Com o Template Method, boa parte de código repetido nas classes filhas passou a ficar na superclasse.
	 *	Sendo que esse método (na superclaesse) faz parte do processo, e a outra parte está na classe filha (qual a superclasse não conhece) 
	 * 
	 * 	https://refactoring.guru/pt-br/design-patterns/template-method	
	 * 	O Template Method é um padrão de projeto comportamental que define o esqueleto de um algoritmo na superclasse mas deixa as subclasses 
	 * 	sobrescreverem etapas específicas do algoritmo sem modificar sua estrutura
	 * 
	 */
	public BigDecimal calcular(Orcamento orcamento) {
		
		Desconto desconto = 
				new DescontoPorQuantidade(
						new DescontoPorValor(
								new SemDesconto()));
		
		
		
		return desconto.aplicar(orcamento);
	}

}
