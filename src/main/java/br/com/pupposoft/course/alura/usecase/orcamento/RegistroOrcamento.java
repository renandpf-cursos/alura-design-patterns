package br.com.pupposoft.course.alura.usecase.orcamento;

import java.util.Map;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;
import br.com.pupposoft.course.alura.exception.SituacaoNaoPermitidaException;
import br.com.pupposoft.course.alura.gateway.HttpAdapter;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RegistroOrcamento {

	/*
	 * ADAPTER: Utilizando interface, é possível haver várias implementações.
	 * Se por acaso haver outro necessidade de usar outro framework de conexão HTTP.
	 * 
	 * Utilizando o padrão, a classe "RegistroOrcamento" não conhece os detalhes da 
	 * implementação de chamadas HTTP.
	 * 
	 * 
	 */
	private HttpAdapter httpAdapter;
	
	public void registrar(Orcamento orcamento) {
		
		if(!orcamento.isFinalizado()) {
			throw new SituacaoNaoPermitidaException();
		}
		
		final String url = "http://api.externa.com.br";
		final Map<String, Object> dados = Map.of(
				"valor", orcamento.getValor(),
				"quantidadeItens", orcamento.getQuantidadeItens());
		
		
		httpAdapter.post(url, dados);
		
	}
	
}
