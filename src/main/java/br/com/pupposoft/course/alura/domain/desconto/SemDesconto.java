package br.com.pupposoft.course.alura.domain.desconto;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;


public class SemDesconto extends Desconto {

	public SemDesconto() {
		super(null);
	}

	@Override
	protected boolean deveAplicar(Orcamento orcamento) {
		return true;
	}

	@Override
	protected BigDecimal calcular(Orcamento orcamento) {
		return BigDecimal.ZERO;
	}
}
