package br.com.pupposoft.course.alura.domain.situacao;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;
import br.com.pupposoft.course.alura.exception.SituacaoNaoPermitidaException;

public abstract class Situacao {

	public void aprovar(Orcamento orcamento) {
		throw new SituacaoNaoPermitidaException();
	}

	public void reprovar(Orcamento orcamento) {
		throw new SituacaoNaoPermitidaException();
	}

	public void analisar(Orcamento orcamento) {
		throw new SituacaoNaoPermitidaException();
	}

	public void finalizar(Orcamento orcamento) {
		throw new SituacaoNaoPermitidaException();
	}
	
	public BigDecimal calcularValorDescontoExtra(Orcamento orcamento) {
		return BigDecimal.ZERO;
	}
}
