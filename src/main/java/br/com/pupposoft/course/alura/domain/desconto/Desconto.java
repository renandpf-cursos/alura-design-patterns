package br.com.pupposoft.course.alura.domain.desconto;

import java.math.BigDecimal;

import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Desconto {

	protected Desconto proximo;

	protected abstract boolean deveAplicar(Orcamento orcamento);
	
	protected abstract BigDecimal calcular(Orcamento orcamento);
	
	public BigDecimal aplicar(Orcamento orcamento) {
		if(deveAplicar(orcamento)) {
			return calcular(orcamento);
		}
		
		return proximo.aplicar(orcamento);
	}
}
