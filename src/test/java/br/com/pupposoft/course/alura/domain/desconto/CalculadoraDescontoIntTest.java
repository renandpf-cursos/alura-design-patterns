package br.com.pupposoft.course.alura.domain.desconto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import br.com.pupposoft.course.alura.domain.orcamento.Item;
import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;

class CalculadoraDescontoIntTest {

	@Test
	void quantidadeMaior() {
		CalculadoraDesconto calc = new CalculadoraDesconto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("16.66")));
		orcamento.adicionarItem(new Item(new BigDecimal("16.66")));
		orcamento.adicionarItem(new Item(new BigDecimal("16.66")));
		orcamento.adicionarItem(new Item(new BigDecimal("16.66")));
		orcamento.adicionarItem(new Item(new BigDecimal("16.66")));
		orcamento.adicionarItem(new Item(new BigDecimal("16.70")));
		
		assertEquals(new BigDecimal("10.00"), calc.calcular(orcamento).setScale(2));
	}
	
	@Test
	void quantidadeMenor() {
		CalculadoraDesconto calc = new CalculadoraDesconto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("100")));
		
		assertEquals(BigDecimal.ZERO, calc.calcular(orcamento));
	}
	
	@Test
	void valorMaior() {
		CalculadoraDesconto calc = new CalculadoraDesconto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("500.01")));
		
		assertEquals(new BigDecimal("50.01"), calc.calcular(orcamento));
	}
	
	@Test
	void valorMenor() {
		CalculadoraDesconto calc = new CalculadoraDesconto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("500.00")));
		
		assertEquals(BigDecimal.ZERO, calc.calcular(orcamento));
	}
}
