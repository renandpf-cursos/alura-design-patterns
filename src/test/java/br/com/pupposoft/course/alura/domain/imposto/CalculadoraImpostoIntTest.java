package br.com.pupposoft.course.alura.domain.imposto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import br.com.pupposoft.course.alura.domain.orcamento.Item;
import br.com.pupposoft.course.alura.domain.orcamento.Orcamento;

class CalculadoraImpostoIntTest {

	@Test
	void icms() {
		CalculadoraImposto calc = new CalculadoraImposto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("100")));
		
		assertEquals(new BigDecimal("10.00"), calc.calcular(orcamento, new ICMS(null)));
	}
	
	@Test
	void iss() {
		CalculadoraImposto calc = new CalculadoraImposto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("100")));
		
		assertEquals(new BigDecimal("6.00"), calc.calcular(orcamento, new ISS(null)));
	}
	
	@Test
	void icmsAndIss() {
		CalculadoraImposto calc = new CalculadoraImposto();
		Orcamento orcamento = new Orcamento();
		orcamento.adicionarItem(new Item(new BigDecimal("100")));
		
		assertEquals(new BigDecimal("16.00"), calc.calcular(orcamento, new ICMS(new ISS(null))));
	}
	
}
