package br.com.pupposoft.course.alura.domain.orcamento;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class ProxyOrcamentoIntTest {

	/*
	 * O pattern Proxy é utilizado não somente para cache, mas também para outros tipos de validações, segurança, performace, etc
	 * Um exemplo famoso é o Hibernate com o uso do Proxy nos relacionamento "lazy"
	 * 
	 */
	
	@Test
	void testProxyDelay() {
		
		Orcamento orcamentoNovo = new Orcamento();
		orcamentoNovo.adicionarItem(new Item(new BigDecimal("500.00")));
		

		assertEquals(new BigDecimal("500.00"), orcamentoNovo.getValor());
		
		OrcamentoProxy proxy = new OrcamentoProxy(orcamentoNovo);
		
		//Agora somente a 1 chamada irá demorar. As demais pegara o valor armazenado
		System.out.println(proxy.getValor());
		System.out.println(proxy.getValor());
		System.out.println(proxy.getValor());

	}
	
}
