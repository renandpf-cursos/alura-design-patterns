package br.com.pupposoft.course.alura.domain.orcamento;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class ReusoOrcamentoAntigoIntTest {

	/*
	 * O Composite é um padrão de projeto estrutural que permite que você componha objetos em estruturas de árvores e 
	 * então trabalhe com essas estruturas como se elas fossem objetos individuais.

	 * COMPOSITE: parecido com o "DECORATOR", mas a diferença é que o decorator as classes
	 * vão se decorando com outras classes. Ja o composite vamos compondo objetos como se fosse uma
	 * estrutura de árvores.
	 * 
	 * 
	 */
	
	
	@Test
	void test() {
		
		//Orçamento antigo que foi reprovado.
		Orcamento orcamentoAntigo = new Orcamento();
		orcamentoAntigo.adicionarItem(new Item(new BigDecimal("200.00")));
		orcamentoAntigo.reprovar();
		
		//Agora queremos criar um novo orcamento mas ainda aproveitando o antigo...
		//Agora é possível
		Orcamento orcamentoNovo = new Orcamento();
		orcamentoNovo.adicionarItem(new Item(new BigDecimal("500.00")));
		orcamentoNovo.adicionarItem(orcamentoAntigo); //Agora NÃO OCORRE ERRO DE COMPILAÇÃO, pois o metodo recebe um tipo "Orcavel"
		

		assertEquals(new BigDecimal("700.00"), orcamentoNovo.getValor());
		
	}
	
}
